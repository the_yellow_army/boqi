package com.qfedu.token_auto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokenAutoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TokenAutoApplication.class, args);
    }

}
